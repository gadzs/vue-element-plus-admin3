export interface BasicDictItem {
  value: number
  label: string
}

export type BasicDict = BasicDictItem[]

export function dictToMap(dict: BasicDict): Map<number, string> {
  const map = new Map<number, string>()

  dict.forEach((item) => {
    map[item.value] = item.label
  })

  return map
}

export function dictToSelectOptions(dict: BasicDict): { label: string; value: number }[] {
  return dict.map((item) => ({
    label: item.label,
    value: item.value
  }))
}
