import { BasicDict, dictToMap } from './basicDict'
import { h } from 'vue'
import { ElTag } from 'element-plus'

export const visibleStatusList = [
  {
    value: 1,
    label: '可见'
  },
  {
    value: 0,
    label: '不可见'
  }
] as BasicDict

export const visibleStatusMap = dictToMap(visibleStatusList)

export const visibleStatusCellTagRender = (v) => {
  return h(
    ElTag,
    {
      type: v === 1 ? 'success' : 'warning'
    },
    () => visibleStatusMap[v]
  )
}
