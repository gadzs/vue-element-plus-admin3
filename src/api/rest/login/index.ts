import request from '@/config/axios'
import type { UserType, LoginResultModel, UserInfoModel } from './types'
import { API_PREX } from '@/utils/constant'
import rs from 'jsrsasign'
// interface RoleParams {
//   roleName: string
// }

export const loginApi = (data: UserType): Promise<IResponse<LoginResultModel>> => {
  const VITE_GLOB_RSA_PUBLIC_KEY = import.meta.env.VITE_GLOB_RSA_PUBLIC_KEY
  const publicKey = rs.KEYUTIL.getKey(
    '-----BEGIN PUBLIC KEY-----\n' + VITE_GLOB_RSA_PUBLIC_KEY + '\n-----END PUBLIC KEY-----'
  ) as rs.RSAKey
  const password = rs.KJUR.crypto.Cipher.encrypt(data.password, publicKey, 'RSA')
  return request.post({ url: API_PREX + '/admin/login', data: { ...data, password } })
  // return { code: '200', data: { token: '123' } }
}

export const loginOutApi = (): Promise<IResponse> => {
  return request.get({ url: API_PREX + '/admin/logout' })
}

export const getUserInfoApi = (): Promise<IResponse<UserInfoModel>> => {
  return request.get({ url: API_PREX + '/admin/user/info' })
}
