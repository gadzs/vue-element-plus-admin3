export type UserLoginType = {
  username: string
  password: string
}

export type UserType = {
  username: string
  password: string
  role: string
  roleId: string
  permissions: string | string[]
}

export type LoginResultModel = {
  token: string
}

export type UserInfoModel = {
  avatar: string
  introduction: string
  name: string
  roles: string[]
  permissions: string[]
  menus: AppCustomRouteRecordRaw[]
}
