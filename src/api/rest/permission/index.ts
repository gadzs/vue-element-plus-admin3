import request from '@/config/axios'
import { API_PREX } from '@/utils/constant'

export const adminPermissionList = (data: any) => {
  return request.get({ url: API_PREX + '/admin/permission/list', params: data })
}

export const adminPermissionAncestors = (id?: string) => {
  return request.get({ url: API_PREX + '/admin/permission/ancestors', params: { id } })
}

export const adminPermissionCheckAvailable = (resource: string, id: string) => {
  return request.get({
    url: API_PREX + '/admin/permission/checkAvailable',
    params: { resource, id }
  })
}

export const adminPermissionCreate = (data: any) => {
  return request.post({ url: API_PREX + '/admin/permission/create', data: data })
}

export const adminPermissionUpdate = (data: any) => {
  return request.post({ url: API_PREX + '/admin/permission/update', data: data })
}

export const adminPermissionDelete = (id: any) => {
  if (id instanceof Array) {
    id = id[0]
  }
  return request.post({ url: API_PREX + '/admin/permission/delete', params: { id } })
}
