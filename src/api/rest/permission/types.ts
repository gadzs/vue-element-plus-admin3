export type PermissionModel = {
  id?: string
  name: string
  description?: string
  resource?: string
  url?: string
  component: string
  icon?: string
  visibleStatus: number
  roleId?: number
  parentId?: string
  sortValue?: number
}
