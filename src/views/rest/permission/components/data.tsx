import { useI18n } from '@/hooks/web/useI18n'
import { CrudSchema } from '@/hooks/web/useCrudSchemas'
import { visibleStatusCellTagRender, visibleStatusList } from '@/dicts/visibleStatus'
import { reactive } from 'vue'
import { TableColumn } from '@/components/Table'
import { formatToDateTime } from '@/utils/dateUtil'
import { Icon } from '@/components/Icon'
import { useValidator } from '@/hooks/web/useValidator'
const { t } = useI18n()
const { required } = useValidator()
export const permissionTableColumns = reactive<CrudSchema[]>([
  {
    field: 'id',
    label: t('restCommon.id'),
    search: {
      hidden: true
    },
    form: {
      hidden: true
    }
  },
  {
    field: 'parentId',
    search: {
      hidden: true
    },
    form: {
      hidden: true
    },
    table: {
      hidden: true
    }
  },
  {
    field: 'name',
    label: t('restCommon.name')
  },
  {
    field: 'resource',
    label: t('restPermission.resource'),
    search: {
      hidden: true
    }
  },
  {
    field: 'visibleStatus',
    label: t('restPermission.visibleStatus'),
    formatter: (_: Recordable, __: TableColumn, cellValue: number) => {
      return visibleStatusCellTagRender(cellValue)
    },
    search: {
      component: 'Select',
      componentProps: {
        style: {
          width: '100%'
        },
        options: visibleStatusList
      }
    },
    form: {
      component: 'Select',
      componentProps: {
        style: {
          width: '100%'
        },
        options: visibleStatusList
      }
    },
    detail: {
      slots: {
        default: (data: any) => {
          return visibleStatusCellTagRender(data.visibleStatus)
        }
      }
    }
  },
  {
    field: 'component',
    label: t('restPermission.component'),
    search: {
      hidden: true
    }
  },
  {
    field: 'createdTime',
    label: t('restCommon.createdTime'),
    search: {
      hidden: true
    },
    formatter: (_: Recordable, __: TableColumn, cellValue: number) => {
      return formatToDateTime(cellValue)
    },
    form: {
      hidden: true
    }
  },
  {
    field: 'updatedTime',
    label: t('restCommon.updatedTime'),
    search: {
      hidden: true
    },
    formatter: (_: Recordable, __: TableColumn, cellValue: number) => {
      return formatToDateTime(cellValue)
    },
    form: {
      hidden: true
    }
  },
  {
    field: 'icon',
    label: t('restCommon.icon'),
    slots: {
      default: (data: any) => {
        const icon = data.row.icon
        if (icon) {
          return (
            <>
              <Icon icon={icon} />
            </>
          )
        } else {
          return null
        }
      }
    },
    search: {
      hidden: true
    }
  }
])

export const permissionFormRules = reactive({
  name: [required()],
  visibleStatus: [required()],
  resource: [{ required: true, message: t('restRules.required'), trigger: 'blur' }],
  component: [{ required: true, message: t('restRules.required'), trigger: 'blur' }]
})
